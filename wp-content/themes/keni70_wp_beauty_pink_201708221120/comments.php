<?php
/**
 * @package WordPress
 * @subpackage Theme_Compat
 * @deprecated 3.0.0
 *
 * This file is here for backward compatibility with old themes and will be removed in a future version
 *
 */

// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) { ?>
	<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'keni'); ?></p>
<?php
	return;
}
?>

<!-- You can start editing here. -->

<?php if ( have_comments() ) : ?>
	<h2 id="comments" class="comment-form-title">
		<?php
			if ( 1 == get_comments_number() ) {
				/* translators: %s: post title */
				printf( __( 'One response to %s', 'keni' ),  '&#8220;' . get_the_title() . '&#8221;' );
			} else {
				/* translators: 1: number of comments, 2: post title */
				printf( _n( '%1$s response to %2$s', '%1$s responses to %2$s', get_comments_number() ),
					number_format_i18n( get_comments_number() ),  '&#8220;' . get_the_title() . '&#8221;' );
			}
		?>
	</h2>

	<ol class="commentlist">
	<?php wp_list_comments( array(
		'style'       => 'ol',
		'format'      => 'html5'
	) ); ?>
	</ol>

<?php if (get_previous_comments_link() != "" || get_next_comments_link() != "") { ?>
	<div class="page-nav-bf">
		<div class="page-nav-prev"><?php previous_comments_link(__('Next Comments', 'keni')) ?></div>
		<div class="page-nav-next"><?php next_comments_link(__('Previous Comments', 'keni')) ?></div>
	</div>
<?php } ?>


 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<!--<p class="nocomments"><?php _e('Comments are closed.'); ?></p>-->

<?php endif;
endif;

if (!function_exists('keni_comment_field')) {
	function keni_comment_field( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}
	add_filter( 'comment_form_fields', 'keni_comment_field' );
}


$args = wp_parse_args(array());

if (!isset( $args['format'])) $args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
$html_req = ( $req ? " required='required'" : '' );
$html5 = 'html5' === $args['format'];

$comments_fields = array(
	'author' => '<div class="comment-form-author">' . '<p><label for="author"><small>' . __('Name','keni') . ( $req ? ' <span class="required">' . __('(required)', 'keni') . '</span>' : '' ) . '</small></label></p>' .
				'<p><input id="author" class="w50" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p></div>',
	'email'  => '<div class="comment-form-email comment-form-mail"><p><label for="email"><small>' . __('Mail (will not be published)','keni') . ( $req ? ' <span class="required">' . __('(required)', 'keni') . '</span>' : '' ) . '</small></label></p> ' .
				'<p><input id="email" class="w50" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div>',
	'url'    => '<div class="comment-form-url"><p><label for="url"><small>' . __( 'Website', 'keni' ) . '</small></label></p>' .
				'<p><input id="url" class="w50" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p></div>',
);

$comments_args = array(
	'fields'               => $comments_fields,
	'comment_field'        => '<p class="comment-form-comment m0-b"><label for="comment"><small>' . _x( 'Comment', 'noun' ) . '</small></label></p><p><textarea id="comment" name="comment" class="w90" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p>',
	'comment_notes_before' => '',
	'comment_notes_after'  => '',
	'submit_button'        => '<p class="al-c"><button name="%1$s" type="submit" id="%2$s" class="%3$s btn btn-form01" value="%4$s"><span>' . __('Submit Comment', 'keni') . '</span></button></p>' ,
	'format'               => 'html5',
);

comment_form($comments_args);
?>