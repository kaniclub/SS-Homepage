jQuery.noConflict();
(function($) {
	$(function() {
		$('input[name="post_category[]"]').on('change', function() {
			var radio = createPrimaryCategoryRadioArea();
			if (radio != null) $('#primary_category_area .inside').html(radio);
		})
	});
	
	function createPrimaryCategoryRadioArea() {
		var radio = '';
		var primary_category = $('input[name="primary_category"]:checked').val();
		var cat_counts = $('input[name="post_category[]"]:checked').length;
		if (cat_counts > 0) {
			var alive = 'n';
			$('input[name="post_category[]"]:checked').map(function() {
				if ($(this).val() == primary_category) alive = 'y';
			})

			$('input[name="post_category[]"]:checked').map(function() {
				if ($(this).val() == primary_category || alive == "n") {
					radio += '<label><input type="radio" name="primary_category" value="'+$(this).val()+'" checked="checked">'+$(this).parent('label').text()+"</label><br />\n";
					alive = "y";
				} else {
					radio += '<label><input type="radio" name="primary_category" value="'+$(this).val()+'">'+$(this).parent('label').text()+"</label><br />\n";
				}
			})
		}
		return radio;
	}
})(jQuery);