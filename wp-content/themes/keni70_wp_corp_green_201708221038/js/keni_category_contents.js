jQuery.noConflict();
(function($) {
	$("#submit").click(function() {
		var set_class = $("#wp-content-wrap").attr('class');
		if (set_class.match(/tmce-active/)) {
			var content_iframe = $('#content_ifr').contents().find('body').html();
			$('#content').val(content_iframe);
			$('#content_ifr').contents().find('body').html('');
		}
	})
})(jQuery);
