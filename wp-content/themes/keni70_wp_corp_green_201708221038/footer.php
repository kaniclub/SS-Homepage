<!--▼サイトフッター-->
<footer class="site-footer">
	<div class="site-footer-in">
	<div class="site-footer-conts">
<?php	$footer = get_globalmenu_keni('footer_menu');
if ( $footer != "") { ?>
		<ul class="site-footer-nav"><?php	echo $footer; ?></ul>
<?php }
$comment = the_keni('footer_comment');
if ($comment != "") { ?>
<div class="site-footer-conts-area"><?php echo do_shortcode(richtext_formats($comment)); ?></div>
<?php } ?>
	</div>
	</div>
	<div class="copyright">
		<p><small>Copyright (C) <?php echo date("Y"); ?> <?php bloginfo('name'); ?> <span>All Rights Reserved.</span></small></p>
	</div>
</footer>
<!--▲サイトフッター-->


<!--▼ページトップ-->
<p class="page-top"><a href="#top"><img class="over" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/common/page-top_off.png" width="80" height="80" alt="<?php _e('To the top', 'keni'); ?>"></a></p>
<!--▲ページトップ-->

</div><!--container-->

<?php wp_footer(); ?>
	
<?php echo do_shortcode(the_keni('body_bottom_text'))."\n"; ?>
</body>
</html>